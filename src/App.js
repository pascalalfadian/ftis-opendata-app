import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import IndexPage from "./components/pages/index/Index";
import GraphPage from "./components/pages/graph/Graph";
import AboutPage from "./components/pages/about/About";
import Appmenu from "./components/common/appmenu/Appmenu";
import UserStore from "./store/userstore";

import { Provider } from 'mobx-react'
function App() {
  return (
    <>
      <Provider userStore={new UserStore()}>
        <Router>
          <Appmenu />
          <Switch>
            <Route exact path="/"><IndexPage /></Route>
            <Route exact path="/graph"><GraphPage /></Route>
            <Route exact path="/about"><AboutPage /></Route>
          </Switch>
        </Router>
      </Provider>
    </>
  );
}

export default App;
