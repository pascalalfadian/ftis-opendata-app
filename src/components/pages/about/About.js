import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';

class About extends Component {
  constructor(props){
    super(props);
  }
  render(){
    return (
      <div className="About">
        Halaman About
      </div>
    )
  }
}
About.propTypes = {
}

export default (observer(About));